import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { TrainerComponent } from './components/pages/trainer/trainer.component';
import { CatalogueComponent } from './components/pages/catalogue/catalogue.component';
import { DetailComponent } from './components/pages/detail/detail.component';
import { RegisterFormComponent } from './components/forms/register-form/register-form.component';
import { CardComponent } from './components/common/card/card.component';

import { AppRoutingModule } from '../app/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './components/common/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    TrainerComponent,
    CatalogueComponent,
    DetailComponent,
    RegisterFormComponent,
    CardComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
