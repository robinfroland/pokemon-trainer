import { Injectable } from '@angular/core';
import { Pokemon } from '../../models/pokemon.model';
import { StorageService } from '../storage/storage.service';
@Injectable({
  providedIn: 'root',
})
export class CollectionService {
  constructor(private storage: StorageService) {}
  initialCollection: Pokemon[] = [];

  initCollection(key: string) {
    const collectionExists = this.storage.getStorage(key);
    if (!collectionExists) {
      this.storage.setStorage(key, this.initialCollection);
    }
  }

  setPokemon(key: string, value: Pokemon): boolean {
    this.initCollection(key);
    const collection: Array<Pokemon> = this.storage.getStorage(key);
    const exists: boolean = collection.some(
      (pokemon) => pokemon.id === value.id
    );

    const pokemon: Pokemon = {
      id: value.id,
      img: value.img,
      name: value.name,
    };

    if (!exists) {
      this.storage.setStorage(key, [pokemon, ...collection]);
      return true;
    } else {
      return false;
    }
  }

  getPokemons(key: string): Array<Pokemon> {
    this.initCollection(key);
    return this.storage.getStorage(key);
  }
}
