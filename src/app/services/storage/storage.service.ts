import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {}

  setStorage(key: string, value: any) {
    const encryptedVal: string = btoa(JSON.stringify(value));
    localStorage.setItem(key, encryptedVal);
    // localStorage.setItem(key, JSON.stringify(value));
  }

  getStorage(key: string) {
    const storedVal: string = localStorage.getItem(key);
    const decryptedVal: string = atob(storedVal);
    if (storedVal) return JSON.parse(decryptedVal);
    // if (storedVal) return JSON.parse(storedVal);
    return false;
  }
}
