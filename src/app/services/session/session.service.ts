import { Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  constructor(private storage: StorageService) {}

  setUser(key: string, value: any) {
    this.storage.setStorage(key, value);
  }

  getUser(key: string) {
    return this.storage.getStorage(key);
  }

  logout(key: string) {
    localStorage.removeItem(key);
  }
}
