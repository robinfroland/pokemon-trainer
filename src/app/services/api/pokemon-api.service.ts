import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment as env } from '../../../environments/environment';
import { Pokemon } from '../../models/pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class PokemonApiService {
  constructor(private http: HttpClient) {}

  pokemon: Pokemon[];

  fetchPokemons(): Promise<Pokemon[]> {
    const params = new HttpParams()
      .set('limit', '"100"')
      .set('offset', '"100"');
    return this.http
      .get<any>(env.pokemonBaseUrl, { params })
      .toPromise()
      .then((response) => response.results)
      .then((results) =>
        results.map((pokemon: Pokemon) => {
          const pokemonId = this.getIdFromUrl(pokemon.url);

          return {
            ...pokemon,
            id: parseInt(pokemonId),
            img: `${env.artworkBaseUrl}/${pokemonId}.png`,
          };
        })
      );
  }

  getIdFromUrl(url: string): string {
    // The pokemons ID is the last pathname in the URL
    // Filter boolean to remove falsies
    const pokemonId: string = url.split('/').filter(Boolean).pop();
    return pokemonId;
  }
}
