import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../../environments/environment';
import { Pokemon } from '../../models/pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class PokemonDetailsApiService {
  constructor(private http: HttpClient) {}

  fetchPokemonDetails(name: string): Promise<Pokemon> {
    return this.http
      .get<any>(`${env.pokemonBaseUrl}/${name}`)
      .toPromise()
      .then((pokemon) => {
        return {
          ...pokemon,
          img: `${env.artworkBaseUrl}/${pokemon.id}.png`,
        };
      });
  }

  getIdFromUrl(url: string): string {
    // The pokemons ID is the last pathname in the URL
    // Filter boolean to remove falsies
    const pokemonId: string = url.split('/').filter(Boolean).pop();
    return pokemonId;
  }
}
