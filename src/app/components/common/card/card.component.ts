import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CardInterface } from '../../../models/card.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() card: CardInterface;
  @Output() onClick: EventEmitter<string> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onCardClicked(): void {
    this.onClick.emit(this.card.title);
  }
}
