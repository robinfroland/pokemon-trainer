import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../services/session/session.service';
import { environment as env } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(private session: SessionService, public router: Router) {}

  ngOnInit(): void {}

  get trainerName(): string {
    return this.session.getUser(env.sessionKey).trainerName;
  }
  handleLogout() {
    this.session.logout(env.sessionKey);
  }
}
