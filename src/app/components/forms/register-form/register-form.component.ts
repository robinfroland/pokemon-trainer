import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../services/session/session.service';
import { environment as env } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
})
export class RegisterFormComponent implements OnInit {
  registerForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
  });

  constructor(private session: SessionService, private router: Router) {
    if (this.session.getUser(env.sessionKey) !== false) {
      this.router.navigateByUrl('/catalogue');
    }
  }

  ngOnInit(): void {}

  get submission() {
    return this.registerForm.get('trainerName');
  }

  onRegisterClick() {
    this.session.setUser(env.sessionKey, this.registerForm.value);
    this.router.navigateByUrl('/catalogue');
  }
}
