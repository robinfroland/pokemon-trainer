import { Component, OnInit } from '@angular/core';
import { PokemonApiService } from '../../../services/api/pokemon-api.service';
import { CardInterface } from 'src/app/models/card.model';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss'],
})
export class CatalogueComponent implements OnInit {
  constructor(private api: PokemonApiService, private router: Router) {}

  loading: boolean;
  cards: CardInterface[] = [];
  pokemons: Pokemon[];

  async ngOnInit() {
    try {
      this.pokemons = await this.api.fetchPokemons();
    } catch (error) {
      console.log(error);
    } finally {
    }

    this.pokemons.map((pokemon) => {
      this.cards.push({
        id: pokemon.id.toString().padStart(3, '0'),
        title: pokemon.name,
        img: pokemon.img,
      });
    });
  }

  handleCardClick(pokemonName: string) {
    this.router.navigate(['/catalogue', pokemonName]);
  }
}
