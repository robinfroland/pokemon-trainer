import { Component, OnInit } from '@angular/core';
import { CardInterface } from 'src/app/models/card.model';
import { Pokemon } from 'src/app/models/pokemon.model';
import { CollectionService } from 'src/app/services/collection/collection.service';
import { environment as env } from '../../../../environments/environment';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss'],
})
export class TrainerComponent implements OnInit {
  cards: CardInterface[] = [];
  pokemons: Pokemon[];
  trainerName: string;

  constructor(
    private collection: CollectionService,
    private session: SessionService,
    private router: Router
  ) {}

  async ngOnInit() {
    this.trainerName = this.session.getUser(env.sessionKey).trainerName;
    this.pokemons = this.collection.getPokemons(env.collectionKey);
    this.pokemons.map((pokemon) => {
      this.cards.push({
        id: pokemon.id.toString().padStart(3, '0'),
        title: pokemon.name,
        img: pokemon.img,
      });
    });
  }

  handleCardClick(pokemonName: string) {
    this.router.navigate(['/catalogue', pokemonName]);
  }
}
