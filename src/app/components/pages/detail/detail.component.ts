import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonDetailsApiService } from 'src/app/services/api/pokemon-details-api.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { CollectionService } from '../../../services/collection/collection.service';
import { environment as env } from '../../../../environments/environment';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  constructor(
    private api: PokemonDetailsApiService,
    private route: ActivatedRoute,
    private collection: CollectionService
  ) {}

  loading: boolean;
  name: string;
  pokemon: Pokemon;
  successfulCollect: boolean;
  exists: boolean;

  async ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('name');

    try {
      this.loading = true;
      this.pokemon = await this.api.fetchPokemonDetails(this.name);
    } catch (e) {
      console.error(e.message);
    } finally {
      this.loading = false;
    }
  }

  onCollect() {
    this.successfulCollect = this.collection.setPokemon(
      env.collectionKey,
      this.pokemon
    );
    if (!this.successfulCollect) {
      this.exists = true;
    }
  }
}
