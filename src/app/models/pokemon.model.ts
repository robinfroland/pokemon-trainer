import { Moves } from './pokemon_details/moves.model';
import { Abilities } from './pokemon_details/abilities.model';
import { Stats } from './pokemon_details/stats.model';
import { Sprites } from './pokemon_details/sprites.model';
import { Types } from './pokemon_details/types.model';

export interface Pokemon {
  id: number;
  name: string;
  img: string;
  abilities?: Abilities[];
  moves?: Moves[];
  stats?: Stats[];
  types?: Types[];
  base_experience?: number;
  url?: string;
  sprites?: Sprites;
  height?: number;
  weight?: number;
}
