export interface CardInterface {
  id?: string;
  img: string;
  title: string;
  description?: string;
  button?: string;
}
