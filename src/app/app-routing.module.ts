import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './components/pages/landing/landing.component';
import { CatalogueComponent } from './components/pages/catalogue/catalogue.component';
import { TrainerComponent } from './components/pages/trainer/trainer.component';
import { DetailComponent } from './components/pages/detail/detail.component';
import { SessionGuard } from './guards/session.guard';

const routes: Routes = [
  {
    path: 'register',
    component: LandingComponent,
  },
  {
    path: 'catalogue',
    component: CatalogueComponent,
    canActivate: [SessionGuard],
  },
  {
    path: 'catalogue/:name',
    component: DetailComponent,
    canActivate: [SessionGuard],
  },
  {
    path: 'profile',
    component: TrainerComponent,
    canActivate: [SessionGuard],
  },
  { path: '', redirectTo: '/register', pathMatch: 'full' },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/register',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
